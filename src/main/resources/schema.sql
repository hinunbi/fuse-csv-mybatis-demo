CREATE TABLE 구매내역 (
                      대표카드번호 VARCHAR2(20) NOT NULL, 
                      회사코드 VARCHAR2(2) NOT NULL, 
                      사업부 VARCHAR2(1) NOT NULL, 
                      매장코드 VARCHAR2(7) NOT NULL, 
                      구매일자 VARCHAR2(8) NOT NULL, 
                      구매번호 VARCHAR2(30) NOT NULL, 
                      구매순번 VARCHAR2(5) NOT NULL, 
                      판매구분 VARCHAR2(1), 
                      스타일 VARCHAR2(50), 
                      색상 VARCHAR2(3), 
                      규격 VARCHAR2(3), 
                      구매수량 NUMBER(7), 
                      구매금액 NUMBER(13), 
                      사용마일리지 NUMBER(13), 
                      적립마일리지 NUMBER(13) 
);

CREATE UNIQUE INDEX PK_구매내역
    ON 구매내역 (
             대표카드번호 ASC,
             회사코드 ASC,
             사업부 ASC,
             매장코드 ASC,
             구매일자 ASC,
             구매번호 ASC,
             구매순번 ASC
        );

ALTER TABLE 구매내역
    ADD
        CONSTRAINT PK_구매내역
            PRIMARY KEY (
                         대표카드번호,
                         회사코드,
                         사업부,
                         매장코드,
                         구매일자,
                         구매번호,
                         구매순번
                );