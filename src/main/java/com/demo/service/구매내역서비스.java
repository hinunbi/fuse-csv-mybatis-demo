package com.demo.service;

import com.demo.model.구매내역;
import org.apache.commons.lang3.RandomUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class 구매내역서비스 {

  @Autowired
  @Qualifier("ERP_MARKET_SQL_SESSION")
  SqlSessionTemplate sqlSession;

  @Transactional("DATASOURCE_ERP_MARKET_TX_MANAGER")
  public int process(구매내역 in) {

    in.set구매번호(Long.toString(RandomUtils.nextLong()));

    int result = sqlSession.insert("mappers.구매내역Mapper.insert", in);
    return result;
  }
}
